pragma solidity ^0.5.0;

contract mmaStars {

    address public admin;
    mapping(address => uint256) public EntreeFees;
    uint256 public PotTotal;
    uint256 public ContestantCount;
    address[] public Contestants;
    address public Winner;
    struct Prediction {
        string WinnerOrDraw;
        string How;
        string Round;
    }
    mapping(address => Prediction) public Predictions;

    constructor () public {
    admin = msg.sender;
    }

    function enterPool() public payable{
        require(EntreeFees[msg.sender] == 0, "Must have not entered");
        require(msg.value == 10 ether, "entry fee is 10 ether");
        uint256 _fee = msg.value;
        EntreeFees[msg.sender] = _fee;
        PotTotal += _fee;
        Contestants.push(msg.sender);
        ContestantCount += 1;
    }

    function setWinner(address _Winner) public {
        require(msg.sender == admin, "only admin can assign winner");
        Winner = _Winner;
    }

    function withdrawPrize() public {
        require(msg.sender == Winner, "only winnder can withdraw pot");
        uint256 withdrawalAmount = PotTotal;
        PotTotal = PotTotal - PotTotal;
        msg.sender.transfer(withdrawalAmount);
    }

    function receivePrediction(string memory _WinnerOrDraw, string memory _How, string memory _Round) public {
        require(EntreeFees[msg.sender] == 10 ether,"You must have put 10 ethere in the pot set predictions");
        Predictions[msg.sender] = Prediction(_WinnerOrDraw, _How, _Round);
    }

}
